<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function __construct()
    {
    }

    public function show(Post $post)
    {
        $comments = $post->comments()->get();
        return view('post.show', compact('post', 'comments'));
    }

    public function create()
    {
        $categories = Category::orderBy('name')->get();
        $posts = Post::all();
        return view('post.create', compact('categories', 'posts'));
    }

    public function store(Request $request)
    {
        $post = new Post;
        $post->name = $request->title;
        $post->content = $request->post_content;
        $fileSize = $request->file('file')->getSize();
        if ($fileSize > 2000000) {
            return back()->with('file_error',"Файл слишком большой =(");
        }
        $file = $request->file('file');
        $filename = $file->getClientOriginalName();
        $file->move('posts', $filename);
        $post->filename = $filename;
        $post->save();
        $post->categories()->attach($request->category);
        return back();
    }

    public function edit(Post $post)
    {
        $categories = Category::orderBy('name')->get();
        $posts = Post::all();
        return view('post.edit', compact('post', 'categories', 'posts'));
    }

    public function update(Request $request)
    {
        $post = Post::find($request->post)->first();
        $post->name = $request->title;
        $post->content = $request->post_content;
        if ($request->file('file')) {
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $file->move('posts', $filename);
            $post->filename = $filename;
        }
        $post->save();
        $post->categories()->detach();
        $post->categories()->attach($request->category);

        return back();
    }

    public function delete(Post $post)
    {
        $post->delete();
        $post->categories()->detach();
        return back();
    }
}
