<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct()
    {
    }

    public function index(Category $category)
    {
        $posts = $category->posts()->paginate(1);
        $categories = Category::all();
        return view('category.posts', compact('categories', 'posts'));

    }

    public function edit(Category $category)
    {
        $categories = Category::all();
        return view('category.edit', compact('category', 'categories'));
    }

    public function update(Request $request)
    {
        $category = Category::find($request->category)->first();
        $category->name = $request->name;
        $category->description = $request->desc;
        $category->save();
        return redirect('category/' . $category->id . '/edit');
    }

    public function create()
    {
        $categories = Category::all();
        return view('category.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $request = $request->input();
        $category = new Category;
        $category->name = $request['name'];
        $category->description = $request['desc'];
        $category->save();
        return redirect(route('category.create'));
    }

    public function delete(Category $category)
    {
        $category->delete();
        $category->posts()->detach();
        return redirect(route('category.create'));
    }
}
