<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function __construct()
    {
    }

    public function create(Request $request)
    {
        $comment = new Comment();
        $comment->author = $request->author;
        $comment->content = $request->comment_content;
        $comment->post_id = $request->post;
        $comment->save();
        return response()->json($comment, 200);
    }

    public function delete(Request $request)
    {
        $comment = Comment::find($request->comment_id);
        if ($comment) {
            $comment->delete();
            return response()->json('done', 200);
        }
        return response()->json('error', 201);

    }

}
