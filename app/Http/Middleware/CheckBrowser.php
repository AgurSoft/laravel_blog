<?php

namespace App\Http\Middleware;

use App\Sessions;
use Closure;

class CheckBrowser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user_agent = $request->header('User-Agent');
        $bname = 'Unknown';

        // Next get the name of the useragent yes seperately and for good reason
        if (preg_match('/MSIE/i', $user_agent) && !preg_match('/Opera/i', $user_agent)) {
            $bname = 'Internet Explorer';
        } elseif (preg_match('/Firefox/i', $user_agent)) {
            $bname = 'Mozilla Firefox';
        } elseif (preg_match('/Chrome/i', $user_agent)) {
            $bname = 'Google Chrome';
        } elseif (preg_match('/Safari/i', $user_agent)) {
            $bname = 'Apple Safari';
        } elseif (preg_match('/Opera/i', $user_agent)) {
            $bname = 'Opera';
        } elseif (preg_match('/Netscape/i', $user_agent)) {
            $bname = 'Netscape';
        }
            $session_id = \Session::getId();
        if ($session_id){

            $user_agent = $bname;
            $user_ip = request()->ip();

            $session = new Sessions();
            $session->id = $session_id;
            $session->user_agent = $user_agent;
            $session->ip_address = $user_ip;
            $session->save();
        }

        return $next($request);
    }
}
