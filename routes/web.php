<?php

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group(['prefix' => 'category'], function () {
    Route::get('create', 'CategoryController@create')->name('category.create');
    Route::post('create', 'CategoryController@store')->name('category.create');
    Route::get('{category}', 'CategoryController@show')->name('category.show');
    Route::get('{category}/posts', 'CategoryController@index')->name('category.show.posts');
    Route::get('{category}/edit', 'CategoryController@edit')->name('category.edit');
    Route::post('edit', 'CategoryController@update')->name('category.update');
    Route::get('{category}/delete', 'CategoryController@delete')->name('category.delete');
});
Route::group(['prefix' => 'comment'], function () {
    Route::get('create', 'CommentController@create')->name('comment.create');
    Route::post('delete', 'CommentController@delete')->name('comment.delete');
});

Route::group(['prefix' => 'post'], function () {
    Route::get('create', 'PostController@create')->name('post.create');
    Route::get('{post}', 'PostController@show')->name('post.show');
    Route::get('{post}/edit', 'PostController@edit')->name('post.edit');
    Route::get('{post}/delete', 'PostController@delete')->name('post.delete');
    Route::post('create', 'PostController@store')->name('post.create');
    Route::post('edit', 'PostController@update')->name('post.update');

});