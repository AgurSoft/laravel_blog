@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="page-header">
            <h1>Добро пожаловать в Блог
            </h1>
        </div>
        <div class="col-sm-3">

            <ul class="list-group">
                @foreach($categories as $category)
                    <li class="list-group-item"><span class="badge">{{$category->posts()->count()}}</span><a
                                href="{{route('category.show.posts',['id'=>$category->id])}}">{{$category->name}}</a></li>
                @endforeach
            </ul>

        </div>
        <div class="col-sm-9">

            @foreach($posts as $post)
                <div class="col-md-12">
                    <div class="thumbnail">
                        <img height="250px" src="{{asset('posts/'.$post->filename) }}"
                             alt="description">
                        <div class="caption">
                            <h3>{{$post->name}}</h3>
                            @guest
                                <a href="{{route('post.show',$post)}}" class="btn btn-primary" role="button">Смотреть</a>
                                @else
                                    <a href="{{route('post.show',$post)}}" class="btn btn-primary" role="button">Смотреть</a>

                                    <a href="{{route('post.edit',$post)}}" class="btn btn-warning" role="button">Редактировать</a>
                                    <a href="{{route('post.delete',$post)}}" class="btn btn-danger" role="button">Удалить</a>

                                    @endguest
                        </div>
                    </div>
                </div>
            @endforeach
            {{$posts->links()}}
        </div>
    </div>
@endsection
