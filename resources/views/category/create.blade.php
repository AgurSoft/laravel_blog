@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{route('category.create')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="">Название Категории</label>
                <input type="text" name="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Описание</label>
                <input type="text" name="desc" class="form-control">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Создать">
            </div>
        </form>
    </div>
    <div class="container">
        <table class="table table-responsive table-bordered">
            <thead>
            <tr>
                <td>Name</td>
                <td>Description</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>{{$category->name}}</td>
                    <td>{{$category->description}}</td>
                    <td><a href="{{route('category.edit',$category)}}" class="btn btn-warning"> Edit</a></td>
                    <td><a href="{{route('category.delete',$category)}}" class="btn btn-danger"> Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

@endsection