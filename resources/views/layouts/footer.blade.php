@php
    $query = DB::table('sessions')->select(DB::raw("user_agent,ip_address, COUNT(*) as count"))->groupBy(['user_agent','ip_address'])->get();
@endphp
<div class="container">
    @foreach($query as $item)
        <div>
            <label for="browser">Браузер:</label><span id="browser">{{$item->user_agent}}</span><br>
            <label for="browser">Ip:</label><span id="browser">{{$item->ip_address}}</span><br>
            <label for="browser">Количество посещений:</label><span id="browser">{{$item->count}}</span>
        </div>
        <hr>

    @endforeach
</div>