@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{route('post.update')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" id="title" value="{{$post->name}}" class="form-control" name="title">
            </div>
            <input type="hidden" name="post" value="{{$post->id}}">
            <div class="form-group">
                <label for="desc">Content:</label>
                <textarea name="post_content" class="form-control" id="desc" cols="10"
                          rows="3">{{$post->content}}</textarea>
            </div>
            @php
                $category_id = $post->categories()->get();
                $category_id  =  $category_id[0]->id;
            @endphp
            <div class="form-group">
                <label for="category">Category:</label>
                <select class="form-control" name="category" id="category">
                    @foreach($categories as $category)
                        <option value="{{$category->id}}"
                                @php
                                    if($category->id == $category_id)
                                    {
                                        echo 'selected';
                                    }

                                @endphp

                        >{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="file">File:</label>
                <input type="file" id="file" class="btn btn-success" name="file">
                <img height="200px" src="{{asset('posts/'.$post->filename)}}" alt="">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success form-control" value="Редактировать">
            </div>
        </form>
    </div>
    <div class="container">
        <table class="table table-responsive table-bordered">
            <thead>
            <tr>
                <td>Image</td>
                <td>Name</td>
                <td>Show</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            </thead>
            <tbody>
            @foreach($posts as $post)
                <tr>
                    <td class="text-center"><img height="100px" src="{{asset('posts/'.$post->filename) }}"
                                                 alt="description"></td>
                    <td>{{$post->name}}</td>
                    <td><a class="btn btn-primary" href="{{$post->id}}">Смотреть</a></td>
                    <td><a class="btn btn-warning" href="{{route('post.edit',$post)}}">Редактировать</a></td>
                    <td><a class="btn btn-danger" href="{{route('post.delete',$post)}}">Удалить</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
@endsection