@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="thumbnail">
                    <img src="{{asset('posts/'.$post->filename)}}" alt="{{$post->name}}">
                    <div class="caption">
                        <h3>{{$post->name}}</h3>
                        <p>{{$post->content}}</p>
                        <p>
                            <a href="{{route('post.edit',$post)}}" class="btn btn-warning"
                               role="button">Редактировать</a>
                            <a href="{{route('post.delete',$post)}}" class="btn btn-danger" role="button">Удалить</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <form name="form">
                    <h3>Ваш Коментарий:</h3>
                    <div class="form-group">
                        <input type="hidden" name="post" value="{{$post->id}}">
                        <label for="author">Автор коментария</label>
                        <input type="text" class="form-control" id="author" name="author">
                        <label for="content">Коментарий</label>
                        <textarea name="content" id="content" class="form-control" cols="10" rows="3"></textarea>
                    </div>
                    <input class="btn btn-success" type="submit" value="Оставить коментарий">
                </form>
            </div>
            <div class="col-sm-12 col-md-12">
                <h3>Коментарии:</h3>
                @foreach($comments as $comment)
                    <div id="{{$comment->id}}">
                        <hr>
                        <div id="" class="media">
                            <div class="media-body">
                                <h4 class="media-heading">{{$comment->author}}</h4>
                                {{$comment->content}}
                            </div>
                            <input type="button" onclick="deleteComment({{$comment->id}})" class="btn btn-danger"
                                   value="Удалить">
                        </div>
                        <hr>
                    </div>
                @endforeach
                <div id="comments">

                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script type="text/javascript">

        function deleteComment(id) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var data = {
                comment_id: id,
                _token: CSRF_TOKEN
            };
            $.ajax({
                type: 'POST',
                url: '/comment/delete',
                data: data,
                dataType: 'json'
            }).done(function (data) {
                console.log(data);
                $('#' + id).hide();
            });
        }

        function prepareAuthor(author) {
            author = author.split(' ');
            var array = [];
            author.forEach(function (item, i, arr) {
                array.push(item.charAt(0).toUpperCase() + item.substr(1));
            });
            if (array.length < 2) {
                alert('Поле "Автор Коментария" должно состоять минимум из 2-х слов');
                return 'false';
            }
            else {
                return array.join(' ');
            }

        }

        $(document).ready(function () {
            $('form').submit(function (event) {
                event.preventDefault();
                var author = $('input[name=author]').val();
                var author = prepareAuthor(author);
                if (author === 'false') {
                    return false;
                }
                var formData = {
                    'author': author,
                    'comment_content': $('textarea[name=content]').val(),
                    'post': $('input[name=post]').val()
                };

                $.ajax({
                    type: 'get',
                    url: '/comment/create',
                    data: formData,
                    dataType: 'json'
                }).done(function (data) {
                    $('#comments').append('<div id="' + data.id + '">' +
                        '<hr>' +
                        '<div id="" class="media">' +
                        '<div class="media-body">' +
                        '<h4 class="media-heading">' + data.author + '</h4>' + data.content +
                        '</div>' +
                        '<input type="button" onclick="deleteComment(' +
                        data.id +
                        ')" class="btn btn-danger" value="asdasdas">' +
                        '</div>' +
                        '<hr>' +
                        '</div>');
                });
                //  event.preventDefault();
            });
        });
    </script>
@endsection